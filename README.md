# Time graph Panel Plugin for Grafana

04/01/2020 : This plugin is under active development. 

This plugin is tested for Grafana 7.0+.  

It is based on [Grafana Panel Plugin Template](https://github.com/grafana/grafana-starter-panel) and  [Discrete Panel Plugin](https://github.com/NatelEnergy/grafana-discrete-panel)

## What is Time graph Panel Plugin Plugin?
A panel plugin is a block of a Grafana dashboard. This plugin allows visualizing data on a time graph. 
This pluginchan displays discrete event data series.
Each data element must have these fields : time, duration, name, serie. 

For more information about panels, refer to the documentation on [Panels](https://grafana.com/docs/grafana/latest/features/panels/panels/)

## Purpose
For the moment, this module is intended to be used only with "Trace Server Protocol" data source.

This panel plugin displays trace data created with LTTng, exposed by a trace server and parsed by "Trace Server Protocol" data source plugin.



## Usage

This plugin is in development and is NOT yet intended for production usage. You can test it by following these steps :

1 . Locate and go in your plugin directory of grafana server
2. Clone this repository inside of plugin directory
3.  Install dependencies
```BASH
yarn install
```
4. Build plugin in production mode
```BASH
yarn build
```
5. Restart the grafana server daemon or restart docker container 


## Development guidelines

There is no official documentation for this plugin, but here are some information about the implementation : 

The main logic is located in **module.ts** file. The main function is 
```JS
onRender(); 
```

- _updateRenderParameters() : parse the data, and calculate the number of rows, and the size of the grid in canvas. Every event is associated to a position on the canvas.
- _updateCanvasSize() : update the canvas size on init and on resize of the window
- _drawEventBlock() : for each positions of the positions array, draw a rectangle representing an event.
- _renderXTimeAxis() : draw the time axis
- _renderLabels() : draw the labels for each events with the name of event
- _renderZoomToRect() :  when a selection is made with the mouse on the canvas, a rectangle is drawed to represent the selection
- _renderCursorLine() : a vertical cursor move whith the mouse when the mouse moves over the canvas

### Learn more
- [Build a panel plugin tutorial](https://grafana.com/tutorials/build-a-panel-plugin)
- [Grafana documentation](https://grafana.com/docs/)
- [Grafana Tutorials](https://grafana.com/tutorials/) - Grafana Tutorials are step-by-step guides that help you make the most of Grafana
- [Grafana UI Library](https://developers.grafana.com/ui) - UI components to help you build interfaces using Grafana Design System
