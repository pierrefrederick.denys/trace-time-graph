import _ from 'lodash';

export class PointInfo {
  val: string;
  startTime: number; // timestamp in ns
  start: number; // timestamp in ns
  lengthX: number; // timestamp in ns

  constructor(val: string, startTime: number, start: number, lengthX: number) {
    this.val = val;
    this.startTime = startTime;
    this.start = start;
    this.lengthX = lengthX;
  }
}

export class LegendValue {
  val: string;
  lengthX = 0; // elapsed time
  count = 0;
  per = 0;

  constructor(val: string) {
    this.val = val;
  }
}

export class DistinctPoints {
  changes: PointInfo[] = [];
  legendInfo: LegendValue[] = [];
  last: PointInfo | null = null;
  asc = false;
  transitionCount = 0;
  distinctValuesCount = 0;
  elapsed = 0;

  constructor(public name) {}

  add(st: number, ts: number, val: string, lengthX: number) {
    this.last = {
      val: val,
      start: st,
      startTime: ts,
      lengthX: lengthX,
    };
    this.changes.push(
      this.last
    ); /* else {
      if (this.changes.lengthX === 1) {
        this.asc = ts > this.last.start;
      }

      if (ts > this.last.start !== this.asc) {
        console.log('skip out of order point', ts, val);
        return;
      }

      // Same value
      if (val === this.last.val) {
        if (!this.asc) {
          this.last.start = ts;
        }
      } else {
        this.last = {
          val: val,
          startTime: st,
          start: ts,
          lengthX: 0,
        };
        this.changes.push(this.last);
      }
    }*/
    /* else if (ts === this.last.start) {
      console.log('skip point with duplicate timestamp', ts, val);
      return;
    } */
  }

  finish(ctrl) {
    if (this.changes.length < 1) {
      console.log('no points found!');
      return;
    }

    if (!this.asc) {
      this.last = this.changes[0];
      _.reverse(this.changes);
    }

    if (!this.last) {
      return;
    }

    // Add a point beyond the controls
    if (this.last.start < ctrl.range.to) {
      const until = ctrl.range.to + 1;
      // let now = Date.now();
      // if(this.last.start < now && ctrl.range.to > now) {
      //   until = now;
      // }

      // This won't be shown, but will keep the count consistent
      this.changes.push({
        val: this.last.val,
        start: until,
        startTime: until,
        lengthX: 0,
      });
    }

    this.transitionCount = 0;
    const distinct = new Map<string, LegendValue>();
    let last: PointInfo = this.changes[0];
    for (let i = 1; i < this.changes.length; i++) {
      const pt = this.changes[i];

      let s = last.start;
      let e = pt.start;
      if (s < ctrl.range.from) {
        s = ctrl.range.from;
      } else if (s < ctrl.range.to) {
        this.transitionCount++;
      }

      if (e > ctrl.range.to) {
        e = ctrl.range.to;
      }

      last.lengthX = e - s;
      if (last.lengthX > 0) {
        if (distinct.has(last.val)) {
          const v = distinct.get(last.val)!;
          v.lengthX += last.lengthX;
          v.count++;
        } else {
          distinct.set(last.val, { val: last.val, lengthX: last.lengthX, count: 1, per: 0 });
        }
      }
      last = pt;
    }

    const elapsed = ctrl.range.to - ctrl.range.from;
    this.elapsed = elapsed;

    distinct.forEach((value: LegendValue, key: any) => {
      value.per = value.lengthX / elapsed;
      this.legendInfo.push(value);
    });
    this.distinctValuesCount = _.size(this.legendInfo);

    if (!ctrl.isTimeline) {
      this.legendInfo = _.orderBy(this.legendInfo, ['length'], ['desc']);
    }
  }

  static combineLegend(data: DistinctPoints[], ctrl: any): DistinctPoints {
    if (data.length === 1) {
      return data[0];
    }

    const merged: DistinctPoints = new DistinctPoints('merged');
    let elapsed = 0;
    const distinct = new Map<string, LegendValue>();
    _.forEach(data, (m: DistinctPoints) => {
      merged.transitionCount += m.transitionCount;
      elapsed += m.elapsed;

      _.forEach(m.legendInfo, (leg: LegendValue) => {
        if (distinct.has(leg.val)) {
          const v = distinct.get(leg.val)!;
          v.lengthX += leg.lengthX;
          v.count += leg.count;
          // per gets recalculated at the end
        } else {
          distinct.set(leg.val, { val: leg.val, lengthX: leg.lengthX, count: leg.count, per: 0 });
        }
      });
    });

    merged.elapsed = elapsed;
    distinct.forEach((value: LegendValue, key: any) => {
      value.per = value.lengthX / elapsed;
      merged.legendInfo.push(value);
    });
    merged.distinctValuesCount = _.size(merged.legendInfo);
    return merged;
  }
}
